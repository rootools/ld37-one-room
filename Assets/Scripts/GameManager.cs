﻿using UnityEngine;
using System.Collections;

public class GameManager : MBSingleton<GameManager> {

    private bool _isFirstLevel = true;

    public void ChangeLevel() {
        _isFirstLevel = !_isFirstLevel;
        if (_isFirstLevel)
            PlayerController.Instance().transform.position = new Vector3(-4.53f, 2f, -3.64f);
        else
            PlayerController.Instance().transform.position = new Vector3(60.4f, -21.5f, -48.1f);
    }
}
