﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerInteractionController : MonoBehaviour {

    private const string COLOR_BOX_PREFABS_PATH = "Prefabs/ColorBoxes/";

    [SerializeField] private Image GazePointer = null;
    [SerializeField] private GameObject PoofParticleObject = null;
    [SerializeField] private PlayerMovementParticleController MovementPointer = null;

    private Dictionary<ColorValue, Color> _gazeColorScheme = new Dictionary<ColorValue, Color>() {
        {ColorValue.None, Color.white },
        {ColorValue.Brown, new Color(204f/255f, 153f/255f, 51f/255f) },
        {ColorValue.Blue, Color.blue }
    };
    private ColorValue _currentInventoryColor;

    void Update () {
        bool colorElementOnFocus = CheckColorElement();
        CheckMovement(colorElementOnFocus);
	}

    private bool CheckColorElement() {
        //RaycastHit hit;

        Transform camTransform = PlayerController.Instance().PlayerCamera.transform;
        RaycastHit[] hits = Physics.RaycastAll(camTransform.position, camTransform.forward, 30f, 1 << LayerMask.NameToLayer("ColorElements"));

        if (hits.Length == 0)
            return false;

        int edgeNotOwnColorCount = hits.Length;

        if (Input.GetButtonDown("Fire1")) {
            List<RaycastHit> hitList = new List<RaycastHit>(hits);
            hitList.Reverse();
            foreach (RaycastHit hit in hitList) {
                ColorElementBase cElem = hit.transform.gameObject.GetComponent<ColorElementBase>();
                if (!CheckColorObjectIsFilled(cElem) && _currentInventoryColor != cElem.ElemColor) {
                    edgeNotOwnColorCount--;
                    continue;
                }

                if (cElem != null) {
                    ClickColorElem(cElem, hit);
                    break;
                }
            }

            if (edgeNotOwnColorCount == 0)
                return false;

            return true;
        }
        return false;
    }

    private bool CheckColorObjectIsFilled(ColorElementBase elem) {
        ColorObject cobj = elem as ColorObject;
        if (cobj == null)
            return true;

        if (cobj.State == ColorObjectState.Edge)
            return false;

        return true;
    }

    private void ClickColorElem(ColorElementBase elem, RaycastHit hit) {
        if (elem.ElemType == ColorElementType.Box)
            ClickColorBox(elem, hit);
        else if (elem.ElemType == ColorElementType.Object)
            ClickColorElement(elem, hit);
        SetGazeColorByCurrentInventoryColor();
    }

    private void ClickColorBox(ColorElementBase elem, RaycastHit hit) {
        if (elem.ElemColor == _currentInventoryColor)
            return;

        ColorBox cbox = elem as ColorBox;
        DropColor();
        SetColor(cbox.ElemColor);
        Destroy(cbox.gameObject);
        SpawnPoof(elem.ElemColor, hit.point);
    }

    private void ClickColorElement(ColorElementBase elem, RaycastHit hit) {
        ColorObject cobj = elem as ColorObject;

        if (cobj.State == ColorObjectState.Fill) {
            DropColor();
            SetColor(cobj.ElemColor);
            cobj.EdgeObject();
            SpawnPoof(elem.ElemColor, hit.point);
        } else if(cobj.State == ColorObjectState.Edge && _currentInventoryColor == cobj.ElemColor && _currentInventoryColor != ColorValue.None) {
            RemoveColor();
            cobj.FillObject();
            SpawnPoof(elem.ElemColor, hit.point);
        }
    }

    private void SetColor(ColorValue color) {
        _currentInventoryColor = color;
    }

    private void DropColor() {
        if (_currentInventoryColor == ColorValue.None)
            return;

        string prefabPath = COLOR_BOX_PREFABS_PATH + _currentInventoryColor.ToString();
        Transform parent = GameManager.Instance().transform;
        ColorBox cbox = Instantiate(Resources.Load<ColorBox>(prefabPath), transform.position, Quaternion.identity, parent) as ColorBox;
        cbox.DropFromPlayer();
        _currentInventoryColor = ColorValue.None;
    }

    private void RemoveColor() {
        _currentInventoryColor = ColorValue.None;
    }

    public void SetGazeColorByCurrentInventoryColor() {
        GazePointer.color = _gazeColorScheme[_currentInventoryColor];
    }

    private void SpawnPoof(ColorValue color, Vector3 pos) {
        GameObject poof = Instantiate(PoofParticleObject, pos, Quaternion.identity, GameManager.Instance().transform) as GameObject;
        PoofController pc = poof.GetComponent<PoofController>();
        pc.Init(_gazeColorScheme[color]);
        PlayerController.Instance().PlayColorInteractSound();
    }

    private void CheckMovement(bool colorElementOnFocus) {
        if (colorElementOnFocus) {
            MovementPointer.Stop();
            return;
        }

        Transform cameraTransform = PlayerController.Instance().PlayerCamera.transform;
        float length = 5f;

        RaycastHit forwardHit;
        if(Physics.Raycast(cameraTransform.position, cameraTransform.forward, out forwardHit, 100f, ~(1 << LayerMask.NameToLayer("ColorElements")))) {
            length = Vector3.Distance(cameraTransform.position, forwardHit.point) - 1f;
        }

        if (length < 2f) {
            MovementPointer.Stop();
            return;
        }
            
        Vector3 cameraRayTopPoint = cameraTransform.position + (cameraTransform.forward * length);

        RaycastHit hit;
        if(Physics.Raycast(cameraRayTopPoint, Vector3.down, out hit)) {
            // Check Trigger layer
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("WayPlatform")) {
                PlatformController platformController = hit.transform.gameObject.GetComponent<PlatformController>();
                // Check Trigger controller
                if (platformController.IsMovementEnable) {
                    MovementPointer.transform.position = hit.point;
                    MovementPointer.Play();

                    if (Input.GetButtonDown("Fire1")) {
                        PlayerController.Instance().MoveTo(hit.point, platformController.IsFinish);
                        MovementPointer.Stop();
                        if (platformController.IsFinish)
                            platformController.FinishCamera.SetActive(true);
                    }
                } else {
                    MovementPointer.Stop();
                }
            } else {
                MovementPointer.Stop();
            }
        } else {
            MovementPointer.Stop();
        }
        
    }

}
