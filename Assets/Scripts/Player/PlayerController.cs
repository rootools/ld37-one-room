﻿using UnityEngine;
using System.Collections;

public class PlayerController : MBSingleton<PlayerController> {

    public Camera PlayerCamera;
    public PlayerInteractionController InteractionController;

    [SerializeField] private AudioSource SFXPlayer = null;
    [SerializeField] private AudioSource MusicPlayer = null;
    [Space]
    [SerializeField] private AudioClip MusicTheme1 = null;
    [SerializeField] private AudioClip MusicTheme2 = null;
    [SerializeField] private AudioClip SFXMovement1 = null;
    [SerializeField] private AudioClip SFXColorInteract1 = null;

    // Mouse
    private const float mouseSensitivity = 100.0f;
    private const float clampAngle = 80.0f;
    private float rotY = 0.0f;
    private float rotX = 0.0f;

    void Start () {
        InitMouseLook();
    }
	
	void Update () {
        if(Cursor.lockState == CursorLockMode.None && Input.GetMouseButtonDown(0))
            Cursor.lockState = CursorLockMode.Locked;

        if (Cursor.lockState == CursorLockMode.Locked && Input.GetButtonDown("Cancel"))
            Cursor.lockState = CursorLockMode.None;

        if(Cursor.lockState == CursorLockMode.Locked)
            MouseLook();
    }

    private void InitMouseLook() {
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
    }

    private void MouseLook() {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        rotY += mouseX * mouseSensitivity * Time.deltaTime;
        rotX += mouseY * mouseSensitivity * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;
    }

    public void MoveTo(Vector3 pos, bool isFinish) {
        PlayMoveSound();
        transform.position = pos + new Vector3(0f, 1f, 0f);
    }

    public void PlayMoveSound() {
        SFXPlayer.clip = SFXMovement1;
        SFXPlayer.pitch = Random.Range(0.8f, 1.2f);
        SFXPlayer.Play();
    }

    public void PlayColorInteractSound() {
        SFXPlayer.clip = SFXColorInteract1;
        SFXPlayer.pitch = Random.Range(0.8f, 1.2f);
        SFXPlayer.Play();
    }

}
