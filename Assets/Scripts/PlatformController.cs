﻿using UnityEngine;
using System.Collections.Generic;

public class PlatformController : MonoBehaviour {

    public bool IsFinish;
    public List<ColorObject> LinkedObjects;
    public GameObject FinishCamera;

    private bool _isMovementEnable;
    public bool IsMovementEnable {
        get {
            return _isMovementEnable;
        }
    }

    void Start() {
        if (LinkedObjects.Count == 0) {
            _isMovementEnable = true;
        } else {
            CheckLinkedObjects();
            SubscribeToObjects();
        }
    }

    void OnDrawGizmos() {
        if (_isMovementEnable)
            Gizmos.color = Color.green;
        else
            Gizmos.color = Color.red;

        Gizmos.DrawWireCube(transform.position, transform.localScale);
    }

    private void CheckLinkedObjects() {
        foreach(ColorObject obj in LinkedObjects) {
            if(obj.State == ColorObjectState.Fill) {
                _isMovementEnable = true;
                return;
            }
        }
        _isMovementEnable = false;
    }

    private void SubscribeToObjects() {
        foreach (ColorObject obj in LinkedObjects) {
            obj.OnStateChange += CheckLinkedObjects;
        }
    }
}