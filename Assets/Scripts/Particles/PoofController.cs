﻿using UnityEngine;

public class PoofController : MonoBehaviour {

    public ParticleSystem PoofEmitter;

    public void Init(Color color) {
        PoofEmitter.startColor = color;
        PoofEmitter.Emit(20);
        Destroy(gameObject, 1f);
    }
	
}
