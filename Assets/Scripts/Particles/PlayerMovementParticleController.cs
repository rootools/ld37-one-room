﻿using UnityEngine;

public class PlayerMovementParticleController : MonoBehaviour {

    [SerializeField] private ParticleSystem Main = null;
    [SerializeField] private ParticleSystem Circle = null;

    public void Play() {
        Main.gameObject.SetActive(true);
        Main.gameObject.SetActive(true);
    }

    public void Stop() {
        Main.gameObject.SetActive(false);
        Main.gameObject.SetActive(false);
    }

}
