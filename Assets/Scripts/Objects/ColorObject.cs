﻿using UnityEngine;
using System.Collections;

public class ColorObject : ColorElementBase {

    public delegate void OnStateChangeDelegate();
    public event OnStateChangeDelegate OnStateChange;

    public ColorObjectState State;
    [SerializeField] private GameObject _fillObject = null;
    [SerializeField] private GameObject _edgeObject = null;

    void Start () {
        SetGameObjectByCurrentState();
    }

    private void SetGameObjectByCurrentState() {
        if(State == ColorObjectState.Edge) {
            _fillObject.SetActive(false);
            _edgeObject.SetActive(true);
        } else {
            _fillObject.SetActive(true);
            _edgeObject.SetActive(false);
        }

        if (OnStateChange != null)
            OnStateChange();
    }

    public void FillObject() {
        State = ColorObjectState.Fill;
        SetGameObjectByCurrentState();
    }

    public void EdgeObject() {
        State = ColorObjectState.Edge;
        SetGameObjectByCurrentState();
    }
	

}
