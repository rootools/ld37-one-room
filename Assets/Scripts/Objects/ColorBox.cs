﻿using UnityEngine;
using System.Collections;

public class ColorBox : ColorElementBase {

    [SerializeField] private Rigidbody RB = null;

    public void DropFromPlayer() {
        PlayerController pc = PlayerController.Instance();

        Vector3 force = pc.PlayerCamera.transform.forward * 50f;
        RB.AddRelativeForce(force + new Vector3(0f, 20f, 0f), ForceMode.Impulse);
    }

}
