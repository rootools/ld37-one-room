﻿using UnityEngine;
using System.Collections;

public class FinishCameraCallback : MonoBehaviour {

	public void Callback() {
        GameManager.Instance().ChangeLevel();
        gameObject.SetActive(false);
    }
}
